#!/usr/bin/env bash

echo "Installing pyenv"
curl https://pyenv.run | bash

echo "Installing Poetry"
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python -

echo "Installing goenv"

echo "Installing tfenv"