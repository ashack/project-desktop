#!/usr/bin/env bash

echo "Getting things setup..."

echo "Install some pre-requisites"

sudo apt install aptitude -y && sudo aptitude install -y curl git

echo "Install pyenv"

# Check if pyenv already exists
if [[ -d "${HOME}/.pyenv" ]]; then
  echo "Seems that pyenv may already be present. Attempting to update..."
  
